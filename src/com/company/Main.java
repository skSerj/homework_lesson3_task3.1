package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //Вывести на консоль те строки, которые содержат одно из заданных ключевых слов (ключевые слова заданы изначально, в массиве например “tree”, “meat”).
        Scanner scan = new Scanner(System.in);
        System.out.println("введите количество строк, которое Вы желаете ввести: ");
        int numRows = Integer.parseInt(scan.nextLine());

        String[] arrayOFRows = new String[numRows];

        for (int i = 0; i < numRows; i++) {
            System.out.printf("Введите строку %d: ", i + 1);
            String row = scan.nextLine();
            arrayOFRows[i] = row;
        }

        String[] listOfKeyWord = {"tree", "meal", "apple"};
        String keyWord = listOfKeyWord[0];
        int maxKeyWord = 3;
        for (int i = 0; i < maxKeyWord; i++) {
            keyWord = listOfKeyWord[i];
            for (int b = 0; b < numRows; b++) {
                if (arrayOFRows[b].contains(keyWord)) {
                    System.out.println("строка, содержащая ключевое слово: " + arrayOFRows[b]);
                }
            }
        }
    }
}